﻿using System;
using System.Collections.Generic;

namespace UnityEngine.XR.iOS
{
    public class TrashcanPlacer : MonoBehaviour
    {
        UnityARAnchorManager unityARAnchorManager;

        // Our score manager - we will pass this reference to any new trash cans that we create
        [SerializeField]
        ScoreManager scoreManager;

        // Our ball thrower - we will initialize this script as soon as the trash can is placed so that we can being throwing
        [SerializeField]
        BallThrower ballThrower;

        /// A prefab for tracking and visualizing detected planes.
        [SerializeField]
        GameObject trackedPlanePrefab;

        /// A model to place when a raycast from a user touch hits a plane.
        [SerializeField]
        GameObject trashCanPrefab;

        /// A gameobject parenting UI for displaying the "searching for planes" snackbar.
        [SerializeField]
        GameObject searchingForPlaneUI;

        // Have we already placed our trashcan?
        [SerializeField]
        bool isTrashCanPlaced;

        void OnEnable()
        {
            // Begin listening to ARKit's frame events to check whether or not we're tracking
            UnityARSessionNativeInterface.ARFrameUpdatedEvent += OnARFrameUpdated;
        }

        void OnDisable()
        {
            // Cease listening to ARKit's frame events
            UnityARSessionNativeInterface.ARFrameUpdatedEvent -= OnARFrameUpdated;
        }

        void OnARFrameUpdated(UnityARCamera cam)
        {
            // If we get a frame update and our tracking state is not normal, show the searching UI
            searchingForPlaneUI.gameObject.SetActive(cam.trackingState != ARTrackingState.ARTrackingStateNormal);
        }

        // Use this for initialization
        void Start()
        {
            // Make sure our screen doesn't dim or go to sleep while we're playing the game
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            // Start scanning and generating AR planes
            BeginGeneratingPlanes();
        }
        void BeginGeneratingPlanes()
        {
            unityARAnchorManager = new UnityARAnchorManager();
            UnityARUtility.InitializePlanePrefab(trackedPlanePrefab);
        }

        void Update()
        {
            // If the user taps on the screen, place the trashcan at that point
            CheckForPlaceTrashCan();
        }

        void CheckForPlaceTrashCan()
        {
            // If the user taps on the screen and we haven't yet placed a trash can...
            if (Input.GetMouseButtonDown(0) && !isTrashCanPlaced)
            {
                // If we're running this in the editor..
                if (Application.isEditor)
                {
                    // Get a point 3 meters in front of the camera to place the trashcan at
                    var infrontOfCamera = Camera.main.transform.TransformPoint(0, -1, 3);

                    // Create a new trashcan by cloning our prefab - and place it at the hit position
                    CreateTrashCan(infrontOfCamera);
                }
                else
                {
                    // If we're running this on a mobile device...

                    // Raycast against the location the player touched to search for planes.
                    RaycastHit hit;

                    // Get the user's pointer position on the screen
                    var pointerPosition = Input.mousePosition;

                    // Define the physics layer that we will raycast onto
                    // var layerMask = LayerMask.NameToLayer("ARKitPlane");

                    // Perform a raycast on our above-defined physics layer. If we get a hit, it's definitely an AR plane
                    if (Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1)), out hit, Mathf.Infinity))
                    {
                        // Get the collider we hit and reference its transform as the anchor we'd like to place our trash can on
                        var anchor = hit.collider.transform;

                        // Create a new trash can and set its position to the hit position of our raycast
                        CreateTrashCan(anchor.position);

                        Debug.Log("Hit a plane on layer " + hit.collider.gameObject.layer);

                        // Hide all of the debug planes by layer - We could just destroy the planes, but there's a weird ARKit 'gotcha' that prevents that
                        Camera.main.cullingMask &= ~(1 << LayerMask.NameToLayer("ARKitPlane"));
                    }
                    else
                    {
                        // If we didn't hit anything, return so that we don't initialize our ball throwe below
                        return;
                    }
                }

                // Initialize the paper thrower
                ballThrower.Init();
            }
        }

        GameObject CreateTrashCan(Vector3 position)
        {
            // Create a new trashcan by cloning our prefab - and place it at the hit position
            var trashCan = Instantiate(trashCanPrefab, position, Quaternion.identity);

            // Add a Trashcan.cs script to our new trash can
            var trashCanScript = trashCan.AddComponent<TrashCan>();

            // Initialize the trashcan, and pass in our score manager so that it has a reference
            trashCanScript.Init(scoreManager);

            // Change the bool so we'll know we've already placed the trashcan
            isTrashCanPlaced = true;

            // Pass back the trash can to the caller of this method
            return trashCan;
        }

        void OnDestroy()
        {
            unityARAnchorManager.Destroy();
        }
    }
}
