# README #

Welcome to Trash Toss for ARCore!

### What is this repository for? ###

* This repository is for YouVisit's AR game 'Trash Toss.' There are two separate repositories for this game. One is for usage with ARKit, the other with ARCore.

### How do I get set up? ###

* Clone the project at this url into a folder locally
* Launch Unity and open the project from the Assets folder
* Within the Unity Editor, open the Asset Store tab
* Search for "ARKit" and download the official ARKit plug-in from Unity
* Import into your project
* Navigate to the Scenes folder and double-click the TrashTossARKit scene

### Who do I talk to? ###

* Questions? Comments? Contact the admin of this repo at addison.yarborough@youvisit.com